#! /bin/bash

# PRP Project
# Copyright (C) 2020 The PRP Project
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

jsbuild='java -jar ./lib/closure.jar --js_output_file ./scripts.js --compilation_level SIMPLE '

jsbuild=$jsbuild'--js ./translation/de.sjs '
jsbuild=$jsbuild'--js ./sjs/translationloader.sjs '
jsbuild=$jsbuild'--js ./sjs/localstoragemanager.sjs '
jsbuild=$jsbuild'--js ./sjs/oauth2.sjs '
jsbuild=$jsbuild'--js ./sjs/logger.sjs '
jsbuild=$jsbuild'--js ./sjs/pluginmanager.sjs '
jsbuild=$jsbuild'--js ./sjs/router.sjs '

jsbuild=$jsbuild'--js ./sjs/init.sjs '

cd plugins

for plugin in `ls -d */`
do

cd ./${plugin::-1}
jsbuild=$jsbuild'--js ./plugins/'${plugin::-1}'/sjs/init.sjs '
echo "$(date) Executing build script for plugin $(echo $plugin)"
eval "/bin/bash ./build.sh"
cd ..

done

cd ..

echo "$(date) Running Closure build command: "$jsbuild
eval $jsbuild

echo "$(date) Compiling SCSS files of framework"
sass ./scss/styles.scss styles.css --style compressed
