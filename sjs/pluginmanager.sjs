/*
        PRP Project
        Copyright (C) 2020  The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

class Pluginmanager {

    constructor() {
        this.plugins = [];
        this.notifications = [];
    }

    register(name, pluginClass) {
        // Create instance from pluginClass
        let pluginObject = new pluginClass();
        this.plugins.push(
            {
                pluginName: name,
                plugin: pluginObject
            }
        )

        // create route to the main page of the plugin
        addRoute(pluginObject.homeUrl, pluginObject.name, pluginObject.filePath);

        // create subroutes of the plugin
        if (pluginObject.subRoutes !== undefined) {
            pluginObject.subRoutes.forEach(subRoute => {
                addRoute(subRoute.subPath, pluginObject.name, subRoute.filePath);
            });
        }

        // Create Main Nav Menu
        this.createNavMenu(pluginObject);
    }

    createNavMenu(plugin) {
        let primarySideNavList = document.getElementById('primarySideNav');
        let mainNavBarTemplate = document.getElementById('mainNavBarItem').content;
        let sideNavItemTemplateCopy = document.importNode(mainNavBarTemplate, true);

        // fill template with values
        sideNavItemTemplateCopy.querySelector('.nav-bar-item-icon').setAttribute('class', plugin.iconClass);
        sideNavItemTemplateCopy.querySelector('.nav-bar-item-name').textContent = plugin.name;
        sideNavItemTemplateCopy.querySelector('.nav-bar-item').setAttribute('href', '#' + plugin.homeUrl);
        sideNavItemTemplateCopy.querySelector('#mainNavBarListItem').setAttribute('data-ranking', plugin.ranking);

        // append value to nav-list
        primarySideNavList.appendChild(sideNavItemTemplateCopy);

        //sort nav-list
        this.sortList(primarySideNavList);
    }

    sortList(ul) {
        let new_ul = ul.cloneNode(false);

        // Add all lis to an array
        let list = [];
        for (let i = ul.childNodes.length; i--;) {
            if (ul.childNodes[i].nodeName === 'LI')
                list.push(ul.childNodes[i]);
        }

        // Sort the lis in ascending order
        list.sort(function (a, b) {
            return parseInt(a.dataset.ranking, 10) -
                parseInt(b.dataset.ranking, 10);
        });

        list.reverse();

        // Add them into the ul in order
        for (let i = 0; i < list.length; i++)
            new_ul.appendChild(list[i]);
        ul.parentNode.replaceChild(new_ul, ul);
    }

    addNotification(title, text, icon, color, link, priority) {
        let newNotification = {
            title: title,
            text: text,
            icon: icon,
            color: color,
            link: link,
            priority: priority
        };
        this.notifications.push(newNotification);
    }

    getNotifications() {
        return this.notifications;
    }

}
