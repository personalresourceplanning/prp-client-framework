/*
        PRP Project
        Copyright (C) 2020  The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

class Logger {

    constructor(clazz, id) {
        if (typeof clazz == "object") {
            this.className = clazz.constructor.name;
        } else {
            this.className = clazz;
        }
        if (typeof id !== 'undefined') {
            this.className = this.className + " #" + id;
        }
    }

    debug(text) {
        this.log(text);
    }

    log(text) {
        console.log(this._buildText(text));
    }

    info(text) {
        console.info(this._buildText(text));
    }

    warn(text) {
        console.warn(this._buildText(text));
    }

    error(text) {
        console.error(this._buildText(text));
    }

    _buildText(text) {
        let currentTime = moment();
        let maxClazzLength = 25;
        let clazz = "";
        if (this.className.length < maxClazzLength) {
            clazz = this.className;
        } else {
            clazz = this.className.substring(0, maxClazzLength - 3) + "...";
        }
        let outputText = currentTime.format("DD.MM.YYYY HH:ss,SSS") + "   [" + clazz + "] ";
        for (i = clazz.length; i <= maxClazzLength; i++) {
            outputText += " ";
        }
        outputText = outputText + text;
        return outputText;
    }

}
